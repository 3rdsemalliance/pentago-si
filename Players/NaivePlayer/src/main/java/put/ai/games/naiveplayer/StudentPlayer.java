/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package put.ai.games.naiveplayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import put.ai.games.Point;
import put.ai.games.game.Board;
import put.ai.games.game.Move;
import put.ai.games.game.Player;

public class StudentPlayer extends Player {

    private Random random = new Random(0xdeadbeef);


    @Override
    public String getName() {
        return "Oskar Kiliańczyk 151863 Kamil Małecki 151861";
    }

    public enum directions {
        // UP,
        RIGHT,
        DOWN,
        // LEFT
    }

    public int inRow(Board b, int posx, int posy, Color color, ArrayList<Point> close){
        int val = 0;
        for (directions d : directions.values()){
            if (d == directions.RIGHT){
                int dl = 0;
                while (b.getState(posx, posy) == color) {
                    close.add(new Point(posx, posy));
                    dl++;
                    posx++;
                    if (b.getState(posx, posy) != color || posx > b.getSize()-1) {
                        val += (1+dl*dl);
                        break;
                    }
                }
            } 
            if (d == directions.DOWN){
                int dl = 0;
                while (b.getState(posx, posy) == color) {
                    close.add(new Point(posx, posy));
                    dl++;
                    posy++;
                    if (b.getState(posx, posy) != color || posy > b.getSize()-1) {
                        val += (1+dl*dl);
                        break;
                    }
                }
            }
        }
        return val;
    }

    public int stateValue(Board b, Color color) {
        Color playerColor = color;
        Color opponentColor = Player.getOpponent(color);
        int valuePlayer = 0;
        int valueOpponent = 0;
        ArrayList<Point> close = new ArrayList<Point>();
        for (int i = 0; i < b.getSize(); i++) {
            for (int j = 0; j < b.getSize(); j++) {
                Point tmpPoint = new Point(i, j);
                if (b.getState(i, j) == playerColor && !close.contains(tmpPoint)) {
                    valuePlayer += inRow(b, i, j, playerColor, close);
                } 
                else if (b.getState(i, j) == opponentColor && !close.contains(tmpPoint)) {
                    valueOpponent += inRow(b, i, j, opponentColor, close);
                }
            }
        }
        if(valueOpponent > 1000){
            if(color == getColor()){
                // System.out.println("Wartość powyżej 1000, na ruchu " + color + " i zwracam MIN_VALUE");
                return Integer.MIN_VALUE;
            }
            else{
                // System.out.println("Wartość powyżej 1000, na ruchu " + color + " i zwracam MAX_VALUE");
                return Integer.MAX_VALUE;
            }
        }
        if (b.getWinner(getColor()) == getColor()) {
            // System.out.println("Znalazłem wygraną, valueOpponent = " + valueOpponent);
            return Integer.MAX_VALUE;
        }
        else if (b.getWinner(getOpponent(getColor())) == getOpponent(getColor())) {
            // System.out.println("Znalazłem wygraną, valueOpponent = " + valueOpponent);
            return Integer.MIN_VALUE;
        }
        return valuePlayer - valueOpponent;
    }
    
    
    Move bestMove;
    
    int MAXDEPTH = 3;
    long startTime;
    
    public double alfabeta(Board board, int depth, Color color){
        List<Move> moves = board.getMovesFor(color);
        Board testBoard = board.clone();
        double thisMoveEval;
        double bestEval;
        Color playerColor = getColor();
        if(color == playerColor){
            bestEval = Integer.MIN_VALUE;
        }
        else{
            bestEval = Integer.MAX_VALUE;
        }
        double threshold = getTime() - getTime() / 10;
        for(Move move : moves){
            if((System.currentTimeMillis() - startTime) > threshold){
                // System.out.println("Czas się kończy, wychodzę z " + bestEval);
                return bestEval;
            }
            testBoard.doMove(move);
            if(depth == 1 || testBoard.getWinner(color) == playerColor){
                thisMoveEval = stateValue(testBoard, color);
            }
            else{
                thisMoveEval = alfabeta(testBoard, depth - 1, getOpponent(color));
            }
            if(thisMoveEval > bestEval && color == playerColor){
                bestEval = thisMoveEval;
                if(depth == MAXDEPTH){
                    bestMove = move;
                }
            }
            if(thisMoveEval < bestEval && color != playerColor){
                bestEval = thisMoveEval;
                if(depth == MAXDEPTH){
                    bestMove = move;
                }
            }
            testBoard.undoMove(move);
        }

        return bestEval;
    }


    @Override
    public Move nextMove(Board b) {
        startTime = System.currentTimeMillis();
        double value = alfabeta(b, MAXDEPTH, getColor());
        // System.out.println("Najlepsza znaleziona ocena: " + value);
        return bestMove;
    }
}
